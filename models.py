from hafahvac import db
from datetime import datetime

class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), nullable=False)
    phone = db.Column(db.String(80), nullable=False)
    body = db.Column(db.Text, nullable = False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())

class Carousel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_name = db.Column(db.String(80), unique = True, nullable=False)
    title = db.Column(db.String(80), nullable=False)
    body = db.Column(db.Text, nullable = False)

class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_name = db.Column(db.String(80), nullable=False)
    title = db.Column(db.String(80), nullable=False)
    body = db.Column(db.Text, nullable = False)