# HAFA Hvac

Coded in 3 days for a local business. Currently live at HAFAHVACinc.com as of 12/22/2019.
All design, development and deployment done by Anthony Oleinik.

# Technologies used

For front end: Bootstrap, Sass, CSS3 Grid, Media Queries
For back end: Python Flask framework, Nginx, Ubuntu, Supervisor, Google SMTP

# Contributing / Using

Improvements / contributions are welcome. This is a showcase project, 
and I got explicit permission from the business to make the code public.
You may clone to get the website and use it as a template. 

# License

MIT liscense. 