from flask import flash, render_template, url_for, make_response, render_template, Blueprint, request, redirect
from hafahvac.admin.forms import SecretForm, CarouselForm, DeleteForm, PhotoForm
from hafahvac.admin.utils import admin_required, allowed_file
from models import Carousel, Photo
from hafahvac import db, app
from werkzeug import secure_filename

admin = Blueprint('admin', __name__)

@admin.route('/admin/login/',  methods=["GET", "POST"])
def admin_login():
    form = SecretForm()
    if form.validate_on_submit():
        if form.password.data == app.config["ADMIN_PASSWORD"]:
            response = make_response(redirect(url_for('admin.routes')))
            response.set_cookie("adminCookie", value = app.config["ADMIN_KEY"])
            return response
        else:
            flash("this password is incorrect!", "danger")
    return render_template('admin/login.html', title = "Admin Login", form = form)

@admin.route('/admin/',  methods=["GET", "POST"])
@admin_required
def routes():
    return render_template('admin/routes.html', title = "admin - routes")

@admin.route('/admin/carousel/',  methods=["GET", "POST"])
@admin_required
def carousel():
    form = CarouselForm()
    delete_form = DeleteForm()
    carousels = Carousel.query.all()
    if form.validate_on_submit():
        if allowed_file(form.image.data.filename):
            filename = secure_filename(form.image.data.filename)
            form.image.data.save('hafahvac/static/images/user-added-images/carousel/' + filename)
            description = ""
            if form.description.data:
                description = form.description.data
            new_image = Carousel(image_name = filename, title = form.title.data, body = description)
            db.session.add(new_image)
            db.session.commit()
            flash("file uploaded!", "success")
            return redirect(url_for('admin.carousel'))
        flash("this file extension is not allowed; only add images in .png, .jpg, .jpeg", "error")
    if delete_form.validate_on_submit():
        to_delete = Carousel.query.filter_by(id = delete_form.id_to_delete.data).first()
        if to_delete:
            db.session.delete(to_delete)
            db.session.commit()
            flash(f"Carousel image '{to_delete.title}' removed!", "success")
            return(redirect(url_for('admin.carousel')))
        else:
            flash(f"no image found with id '{delete_form.id_to_delete.data}'", "danger")
            return(redirect(url_for('admin.carousel')))
    return render_template('admin/carousel.html', title = "admin - carousel", form = form, delete_form = delete_form, carousels = carousels)

@admin.route('/admin/photos/',  methods=["GET", "POST"])
@admin_required
def photos():
    form = PhotoForm()
    delete_form = DeleteForm()
    photos = Photo.query.all()
    if form.validate_on_submit():
        if allowed_file(form.image.data.filename):
            filename = secure_filename(form.image.data.filename)
            form.image.data.save('hafahvac/static/images/user-added-images/photos/' + filename)
            description = ""
            if form.description.data:
                description = form.description.data
            new_image = Photo(image_name = filename, title = form.title.data, body = description)
            db.session.add(new_image)
            db.session.commit()
            flash("file uploaded!", "success")
            return redirect(url_for('admin.photos'))
        flash("this file extension is not allowed; only add images in .png, .jpg, .jpeg", "error")
    if delete_form.validate_on_submit():
        to_delete = Photo.query.filter_by(id = delete_form.id_to_delete.data).first()
        if to_delete:
            db.session.delete(to_delete)
            db.session.commit()
            flash(f"Photo '{to_delete.title}' removed!", "success")
            return(redirect(url_for('admin.photos')))
        else:
            flash(f"no image found with id '{delete_form.id_to_delete.data}'", "danger")
            return(redirect(url_for('admin.photos')))
    return render_template('admin/photos.html', title = "admin - photos", form = form, delete_form = delete_form, photos = photos)


