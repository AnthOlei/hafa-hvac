from flask_wtf import FlaskForm
from wtforms.validators import InputRequired
from flask_wtf.file import FileField, FileRequired
from wtforms import PasswordField, SubmitField, StringField, TextAreaField

class SecretForm(FlaskForm):
    password = PasswordField('Name')
    submit = SubmitField('Login')

class CarouselForm(FlaskForm):
    image = FileField([FileRequired()])
    title = StringField('Title', [InputRequired()])
    description = TextAreaField('body')
    submit = SubmitField('Submit')

class PhotoForm(FlaskForm):
    image = FileField([FileRequired()])
    title = StringField('Title', [InputRequired()])
    description = TextAreaField('body')
    submit = SubmitField('Submit')

class DeleteForm(FlaskForm):
    id_to_delete = StringField('Delete', [InputRequired()])  
    submit = SubmitField('Submit')  