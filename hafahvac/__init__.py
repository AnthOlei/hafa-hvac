from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from hafahvac.config import Config

app = Flask(__name__)

@app.context_processor
def inject_globals():
    return dict(app_name="H&#8226;A&#8226;F&#8226;A HVAC")

app.config.from_object(Config)
db = SQLAlchemy(app)
mail = Mail(app)

db.init_app(app)

from hafahvac.errors.handlers import errors
from hafahvac.site.routes import site 
from hafahvac.legal.routes import legal
from hafahvac.admin.routes import admin

app.register_blueprint(errors)
app.register_blueprint(site)
app.register_blueprint(legal)
app.register_blueprint(admin)