from flask import Blueprint, render_template, jsonify, flash, redirect, url_for, make_response, request
from hafahvac.site.forms import ContactForm
from hafahvac.site.utils import send_email
from hafahvac import db, app
from models import *

site = Blueprint('site', __name__)

@site.route('/',  methods=["GET", "POST"])
def index():
    form = ContactForm()
    carousels = Carousel.query.all()
    if form.validate_on_submit():
        phone = "(Not Entered)"
        if form.phone.data:
            phone = form.phone.data
        inquery = Contact(email = form.email.data, phone = phone, body = form.body.data)
        db.session.add(inquery)
        db.session.commit()
        send_email(form.email.data, form.body.data, form.phone.data)
        flash("Your message has been recived! please allow 24-48 business hours for a response.", "success")
        return redirect(url_for('site.index'))
    return render_template('index.html', title = "HAFA HVAC", form = form, carousels = carousels)

@site.route('/photos/')
def photos():
    photos = Photo.query.all()
    return render_template('photos.html', title = "HAFA HVAC - Photos", photos = photos)

@site.route('/contact/', methods=["GET", "POST"])
def contact():
    form = ContactForm()
    if form.validate_on_submit():
        inquery = Contact(email = form.email.data, phone = form.phone.data, body = form.body.data)
        db.session.add(inquery)
        send_email(form.email.data, form.body.data, form.phone.data)
        db.session.commit()
        flash("Your message has been recived! please allow 24-48 business hours for a response.", "success")
        return redirect(url_for('site.contact'))
    return render_template('contact.html', title = "HAFA HVAC - Contact", form = form)