from flask_mail import Message
from hafahvac import mail
from datetime import datetime, timedelta

def send_email(email, body, phone):
    print('function called')
    msg = Message(f"Inquery posted for HAFA HVAC {datetime.utcnow() - timedelta(hours=6)}",
                  sender="myflaskemailsender@gmail.com",
                  recipients=["antholeinik@gmail.com"])
    msg.body = f"new inquery sent by: {email}, with phone number: {phone}. \n\n Inquery: {body}"
    print('message composed')
    try:
        mail.send(msg)
    except Exception as e:
        print(e)
    print('message sent')