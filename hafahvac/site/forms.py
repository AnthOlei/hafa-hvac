from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, TextAreaField, DecimalField
from wtforms.validators import DataRequired, Length, Email, EqualTo

class ContactForm(FlaskForm):
    name = StringField('Name')
    email = StringField('Email', [DataRequired(), Email()])
    phone = StringField('Phone')
    body = TextAreaField('body', [DataRequired()])
    submit = SubmitField('Submit Inquery')
