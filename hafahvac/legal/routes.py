from flask import Blueprint, render_template, jsonify

legal = Blueprint('legal', __name__)

@legal.route('/legal/cookiePolicy/')
def cookiePolicy():
    return render_template('cookiePolicy.html', title = "Cookie Policy")